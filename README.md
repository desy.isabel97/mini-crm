**Langkah Mengakses Proyek**
1. Clone Proyek

    git clone git@gitlab.com:desy.isabel97/mini-crm.git

2. Mengakses Direktori Proyek

    cd ManajemenPegawai

3. Menginstall composer

    composer install

4. Mengatur database pada file .env

    DB_CONNECTION=mysql

    DB_HOST=127.0.0.1

    DB_PORT=3306

    DB_DATABASE=manajemen-pegawai

    DB_USERNAME= your_username

    DB_PASSWORD= your_password

5. Mengatur mail untuk mailtrap.io pada file .env

    MAIL_MAILER=smtp

    MAIL_HOST=smtp.mailtrap.io

    MAIL_PORT=2525

    MAIL_USERNAME=87afb2e6fa1f0a

    MAIL_PASSWORD=645a8b69c562bc

    MAIL_ENCRYPTION=null

    MAIL_FROM_ADDRESS= your_email

    MAIL_FROM_NAME="${APP_NAME}" 

6. Melakukan migrate untuk database

    php artisan:migrate

7. Melakukan seed untuk data admin

    php artisan db:seed

8. Menghubungkan link storage

    php artisan storage:link

9. Menjalankan artisan

    php artisan serve

10. Menjalankan queue untuk proses mengirim email

    php artisan queue:work

11. Proyek dapat dijalankan

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
