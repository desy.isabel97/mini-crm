<?php

namespace Database\Factories;

use App\Models\Sell;
use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;
use Faker\Generator as Faker;

class SellFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Sell::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'created_date' => Carbon::now(),
            'item' => $this->faker->numberBetween(1, 10),
            'price' => $this->faker->numberBetween(10000, 10000000),
            'discount' => rand(0.05, 0.90)
        ];
    }
}
