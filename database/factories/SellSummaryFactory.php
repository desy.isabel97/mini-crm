<?php

namespace Database\Factories;

use App\Models\SellSummary;
use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;
use Faker\Generator as Faker;

class SellSummaryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SellSummary::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'last_update' => Carbon::now(),
            'price_total' => $this->faker->numberBetween(100000, 100000000),
            'discount_total' => rand(0.05, 0.90),
            'total' => $this->faker->numberBetween(100000, 100000000),

        ];
    }
}
