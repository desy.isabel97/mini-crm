<?php

namespace Database\Factories;

use App\Models\Companies;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;

class CompaniesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Companies::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => 'Water Blue Corp.',
            'email' => 'waterblue@mail.com',
            'website' => 'waterblue.com',
            'logo' => '/public/logo/avatar.jpg',
            'created_by_id' => 1,
            'updated_by_id' => 1,
            'domain' => 'companytest',
        ];
    }
}
