employees
=========

| first_name | last_name |         email          |    phone     | company |   password   |   |   |   |   |   |
|------------|-----------|------------------------|--------------|---------|--------------|---|---|---|---|---|
| Gisel      | Marana    | marana.gicell@mail.com | 087633927821 | 1       | bcrypt(1234) |   |   |   |   |   |
| Putra      | Krhisna   | k.putra@gmail.com      | 081212436671 | 2       | bcrypt(1234) |   |   |   |   |   |
| Wiranasa   | Kalenda   | wiraenda@mail.co.id    | 089822387878 | 3       | bcrypt(1234) |   |   |   |   |   |
| Haekal     | Surya     | haesurya@mail.com      | 083312128976 | 4       | bcrypt(1234) |   |   |   |   |   |
| Tiana      | Hera      | tianahera@mail.com     | 89921892921  | 5       | bcrypt(1234) |   |   |   |   |   |
| Laura      | Sinan     | laurasinan@mail.com    | 087633927821 | 6       | bcrypt(1234) |   |   |   |   |   |
| Merry      | Perim     | merryperim@mail.com    | 081212436671 | 7       | bcrypt(1234) |   |   |   |   |   |
| Bella      | Aljen     | bellaaljen@mail.com    | 089822387878 | 8       | bcrypt(1234) |   |   |   |   |   |
| Maraka     | Wijaya    | marakawijaya@mail.com  | 083312128976 | 9       | bcrypt(1234) |   |   |   |   |   |
| Arnan      | Hera      | arnanhera@mail.com     | 89921892922  | 10      | bcrypt(1234) |   |   |   |   |   |
| Nahen      | Sinan     | nahensinan@mail.com    | 087633927821 | 1       | bcrypt(1234) |   |   |   |   |   |
| Mahendra   | Perim     | mahendraperim@mail.com | 081212436671 | 2       | bcrypt(1234) |   |   |   |   |   |
| Jenan      | Aljen     | jenanaljen@mail.com    | 089822387878 | 3       | bcrypt(1234) |   |   |   |   |   |
| Cecil      | Wijaya    | cecilwijaya@mail.com   | 083312128976 | 4       | bcrypt(1234) |   |   |   |   |   |
| Melia      | Hera      | meliahera@mail.com     | 89921892923  | 5       | bcrypt(1234) |   |   |   |   |   |
| Susan      | Sinan     | susansinan@mail.com    | 087633927821 | 6       | bcrypt(1234) |   |   |   |   |   |
| Tasya      | Perim     | tasyaperim@mail.com    | 081212436671 | 7       | bcrypt(1234) |   |   |   |   |   |
| Kalyn      | Aljen     | kalynaljen@mail.com    | 089822387878 | 8       | bcrypt(1234) |   |   |   |   |   |
| Cinta      | Wijaya    | cintawijaya@mail.com   | 083312128976 | 9       | bcrypt(1234) |   |   |   |   |   |
| Nikasya    | Hera      | nikasyahera@mail.com   | 89921892924  | 10      | bcrypt(1234) |   |   |   |   |   |
(20 rows)

