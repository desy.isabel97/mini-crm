companies
=========

|        name        |        email        |                   logo                   |      website       |     domain     |   |   |   |   |
|--------------------|---------------------|------------------------------------------|--------------------|----------------|---|---|---|---|
| GT Company         | gt@company.com      | https://lorempixel.com/400/200/business/ | gtcompany.com      | gtcompany      |   |   |   |   |
| Erlangga Corp      | erlangga@corp.com   | https://lorempixel.com/400/200/business/ | erlanggacorp.com   | erlanggacorp   |   |   |   |   |
| Joy CT.            | joy@ct.com          | https://lorempixel.com/400/200/business/ | joyct.com          | joyct          |   |   |   |   |
| Pre Star Main Env. | prestar@mainenv.com | https://lorempixel.com/400/200/business/ | prestarmainenv.com | prestarmainenv |   |   |   |   |
| Mega Group         | megagroup@mail.com  | https://lorempixel.com/400/200/business/ | megagroup.com      | megagroup      |   |   |   |   |
| Company 1          | company1@mail.com   | https://lorempixel.com/400/200/business/ | company1.com       | company1       |   |   |   |   |
| Company 2          | company1@mail.com   | https://lorempixel.com/400/200/business/ | company2.com       | company2       |   |   |   |   |
| Company 3          | company1@mail.com   | https://lorempixel.com/400/200/business/ | company1.com       | company3       |   |   |   |   |
| Company 4          | company1@mail.com   | https://lorempixel.com/400/200/business/ | company2.com       | company4       |   |   |   |   |
| Company 5          | company1@mail.com   | https://lorempixel.com/400/200/business/ | company1.com       | company5       |   |   |   |   |
| Company 6          | company1@mail.com   | https://lorempixel.com/400/200/business/ | company2.com       | company6       |   |   |   |   |
| Company 7          | company1@mail.com   | https://lorempixel.com/400/200/business/ | company1.com       | company7       |   |   |   |   |
| Company 8          | company1@mail.com   | https://lorempixel.com/400/200/business/ | company2.com       | company8       |   |   |   |   |
| Company 9          | company1@mail.com   | https://lorempixel.com/400/200/business/ | company1.com       | company9       |   |   |   |   |
| Company 10         | company1@mail.com   | https://lorempixel.com/400/200/business/ | company2.com       | company10      |   |   |   |   |
| Company 11         | company1@mail.com   | https://lorempixel.com/400/200/business/ | company1.com       | company11      |   |   |   |   |
| Company 12         | company1@mail.com   | https://lorempixel.com/400/200/business/ | company2.com       | company12      |   |   |   |   |
| Company 13         | company1@mail.com   | https://lorempixel.com/400/200/business/ | company1.com       | company13      |   |   |   |   |
| Company 14         | company1@mail.com   | https://lorempixel.com/400/200/business/ | company2.com       | company14      |   |   |   |   |
| Company 15         | company1@mail.com   | https://lorempixel.com/400/200/business/ | company1.com       | company15      |   |   |   |   |
| Company 16         | company1@mail.com   | https://lorempixel.com/400/200/business/ | company2.com       | company16      |   |   |   |   |
(21 rows)

