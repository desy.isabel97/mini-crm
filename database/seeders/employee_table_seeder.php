<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Employees;
use bfinlay\SpreadsheetSeeder\SpreadsheetSeeder;

class employee_table_seeder extends SpreadsheetSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->worksheetTableMapping = ['employees' => 'employees'];
        
        parent::run();


    }
}
