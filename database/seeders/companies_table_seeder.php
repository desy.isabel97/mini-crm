<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Companies;
use bfinlay\SpreadsheetSeeder\SpreadsheetSeeder;

class companies_table_seeder extends SpreadsheetSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
		$this->worksheetTableMapping = ['companies' => 'companies'];        
        parent::run();
    }
}
