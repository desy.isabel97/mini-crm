<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use bfinlay\SpreadsheetSeeder\SpreadsheetSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        // $this->call(employee_table_seeder::class);;
        // $this->call(companies_table_seeder::class);
        $this->call([
            SpreadsheetSeeder::class,
        ]);
    }
}
