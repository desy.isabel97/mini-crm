<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Sell;

class SellSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sell::factory()->create(['created_date' => '2021-07-26 08:16:45', 'employee' => 1]);
        Sell::factory()->create(['created_date' => '2021-07-26 09:16:45', 'employee' => 1]);
        
        Sell::factory()->create(['created_date' => '2021-07-27 08:16:45', 'employee' => 1]);
        Sell::factory()->create(['created_date' => '2021-07-27 09:16:45', 'employee' => 1]);

        Sell::factory()->create(['created_date' => '2021-07-27 10:16:45', 'employee' => 2]);
        Sell::factory()->create(['created_date' => '2021-07-27 11:16:45', 'employee' => 2]);
        Sell::factory()->create(['created_date' => '2021-07-27 12:16:45', 'employee' => 2]);

        Sell::factory()->create(['created_date' => '2021-07-27 13:16:45', 'employee' => 3]);
        Sell::factory()->create(['created_date' => '2021-07-27 14:16:45', 'employee' => 3]);
        
        Sell::factory()->create(['created_date' => '2021-07-28 08:16:45', 'employee' => 4]);
        Sell::factory()->create(['created_date' => '2021-07-28 09:16:45', 'employee' => 4]);

        Sell::factory()->create(['created_date' => '2021-07-28 10:16:45', 'employee' => 5]);
        Sell::factory()->create(['created_date' => '2021-07-28 11:16:45', 'employee' => 5]);
        Sell::factory()->create(['created_date' => '2021-07-28 12:16:45', 'employee' => 5]);

        Sell::factory()->create(['created_date' => '2021-07-29 08:16:45', 'employee' => 6]);
        Sell::factory()->create(['created_date' => '2021-07-29 09:16:45', 'employee' => 6]);
        
        Sell::factory()->create(['created_date' => '2021-07-29 10:16:45', 'employee' => 7]);
        Sell::factory()->create(['created_date' => '2021-07-29 11:16:45', 'employee' => 7]);

        Sell::factory()->create(['created_date' => '2021-07-29 12:16:45', 'employee' => 8]);
        Sell::factory()->create(['created_date' => '2021-07-29 13:16:45', 'employee' => 8]);
        Sell::factory()->create(['created_date' => '2021-07-29 14:16:45', 'employee' => 8]);
    }
}
