<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SellSummary;

class SellSummarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        SellSummary::factory()->create(['date' => '2021-07-26', 'employee' => 1, 'created_date' => '2021-07-26 08:16:45']);

        SellSummary::factory()->create(['date' => '2021-07-27', 'employee' => 1, 'created_date' => '2021-07-27 09:16:45']);

        SellSummary::factory()->create(['date' => '2021-07-27', 'employee' => 2, 'created_date' => '2021-07-27 10:16:45']);

        SellSummary::factory()->create(['date' => '2021-07-27', 'employee' => 3, 'created_date' => '2021-07-27 11:16:45']);

        SellSummary::factory()->create(['date' => '2021-07-28', 'employee' => 4, 'created_date' => '2021-07-28 08:16:45']);

        SellSummary::factory()->create(['date' => '2021-07-28', 'employee' => 5, 'created_date' => '2021-07-28 09:16:45']);

        SellSummary::factory()->create(['date' => '2021-07-29', 'employee' => 6, 'created_date' => '2021-07-29 08:16:45']);

        SellSummary::factory()->create(['date' => '2021-07-29', 'employee' => 7, 'created_date' => '2021-07-29 09:16:45']);

        SellSummary::factory()->create(['date' => '2021-07-29', 'employee' => 8, 'created_date' => '2021-07-29 10:16:45']);
    }
}
