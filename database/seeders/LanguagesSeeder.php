<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\TranslationLoader\LanguageLine;

class LanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'welcome',
        //     'text' => ['en' => 'Welcome!', 'id' => 'Selamat Datang!'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Companies',
        //     'text' => ['en' => 'Companies', 'id' => 'Perusahaan'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Employees',
        //     'text' => ['en' => 'Employees', 'id' => 'Pegawai'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Companies List',
        //     'text' => ['en' => 'Companies List', 'id' => 'Daftar Perusahaan'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Employees List',
        //     'text' => ['en' => 'Employees List', 'id' => 'Daftar Pegawai'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Company',
        //     'text' => ['en' => 'Company', 'id' => 'Perusahaan'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Employee',
        //     'text' => ['en' => 'Employee', 'id' => 'Pegawai'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Login',
        //     'text' => ['en' => 'Login', 'id' => 'Masuk'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Logout',
        //     'text' => ['en' => 'Logout', 'id' => 'Keluar'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Name',
        //     'text' => ['en' => 'Name', 'id' => 'Nama'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'First Name',
        //     'text' => ['en' => 'First Name', 'id' => 'Nama Depan'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Last Name',
        //     'text' => ['en' => 'Last Name', 'id' => 'Nama Belakang'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Email',
        //     'text' => ['en' => 'Email', 'id' => 'Alamat Surel'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Phone',
        //     'text' => ['en' => 'Phone', 'id' => 'No. Telepon'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Website',
        //     'text' => ['en' => 'Website', 'id' => 'Situs web'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Logo',
        //     'text' => ['en' => 'Logo', 'id' => 'Logo'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'View',
        //     'text' => ['en' => 'View', 'id' => 'Lihat'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Detail',
        //     'text' => ['en' => 'Detail', 'id' => 'Detail'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Add',
        //     'text' => ['en' => 'Add', 'id' => 'Tambah'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Edit',
        //     'text' => ['en' => 'Edit', 'id' => 'Ubah'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Delete',
        //     'text' => ['en' => 'Delete', 'id' => 'Hapus'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Action',
        //     'text' => ['en' => 'Action', 'id' => 'Aksi'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Confirmation',
        //     'text' => ['en' => 'Confirmation', 'id' => 'Konfirmasi'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'deleteConf',
        //     'text' => ['en' => 'Are you sure you want to remove this data?', 'id' => 'Anda yakin ingin menghapus data ini?'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Cancel',
        //     'text' => ['en' => 'Cancel', 'id' => 'Batal'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'maxSize',
        //     'text' => ['en' => 'Max. Size', 'id' => 'Ukuran Maksimal'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'minDim',
        //     'text' => ['en' => 'Min. Dimension', 'id' => 'Dimensi Minimal'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'createdAt',
        //     'text' => ['en' => 'Created Date', 'id' => 'Tanggal Ditambahkan'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Domain',
        //     'text' => ['en' => 'Domain Name', 'id' => 'Nama Domain'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Items List',
        //     'text' => ['en' => 'Items List', 'id' => 'Daftar Barang'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Item',
        //     'text' => ['en' => 'Item', 'id' => 'Barang'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Price',
        //     'text' => ['en' => 'Price', 'id' => 'Harga'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'SellDetail',
        //     'text' => ['en' => 'Sell Detail', 'id' => 'Detail Penjualan'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Discount',
        //     'text' => ['en' => 'Discount', 'id' => 'Diskon'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'SellList',
        //     'text' => ['en' => 'Sell List', 'id' => 'Daftar Penjualan'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Sell',
        //     'text' => ['en' => 'Sell', 'id' => 'Penjualan'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'SellSummaryDetail',
        //     'text' => ['en' => 'Sell Summary Detail', 'id' => 'Detail Ringkasan Penjualan'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'AddCompany',
        //     'text' => ['en' => 'Add New Company', 'id' => 'Tambah Perusahaan'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'NewPassword',
        //     'text' => ['en' => 'New Password', 'id' => 'Kata Sandi Baru'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'optional',
        //     'text' => ['en' => 'optional', 'id' => 'opsional'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Company Detail',
        //     'text' => ['en' => 'Company Detail', 'id' => 'Detail Perusahaan'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'ItemName',
        //     'text' => ['en' => 'Item Name', 'id' => 'Nama Barang'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'ItemDetail',
        //     'text' => ['en' => 'Item Detail', 'id' => 'Detail Barang'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Timezone',
        //     'text' => ['en' => 'Choose Timezone', 'id' => 'Pilih Zona Waktu'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'AddSell',
        //     'text' => ['en' => 'Add New Sell', 'id' => 'Tambah Data Penjualan'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'EditSell',
        //     'text' => ['en' => 'Edit Sell', 'id' => 'Ubah Data Penjualan'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'SellSummary',
        //     'text' => ['en' => 'Sell Summary', 'id' => 'Ringkasan Penjualan'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Date',
        //     'text' => ['en' => 'Date', 'id' => 'Tanggal'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'PriceTotal',
        //     'text' => ['en' => 'Total Price', 'id' => 'Total Harga'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'Total',
        //     'text' => ['en' => 'Total', 'id' => 'Total'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'ChooseEmployee',
        //     'text' => ['en' => 'Choose Employee', 'id' => 'Pilih Pegawai'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'ChooseCompany',
        //     'text' => ['en' => 'Choose Company', 'id' => 'Pilih Perusahaan'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'FromDate',
        //     'text' => ['en' => 'From Date', 'id' => 'Tanggal Awal'],
        // ]);
        // LanguageLine::create([
        //     'group' => 'text',
        //     'key' => 'ToDate',
        //     'text' => ['en' => 'To Date', 'id' => 'Tanggal Akhir'],
        // ]);
        LanguageLine::create([
            'group' => 'text',
            'key' => 'DiscountTotal',
            'text' => ['en' => 'Discount Total', 'id' => 'Total Diskon'],
        ]);
    }
}
