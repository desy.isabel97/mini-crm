<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSellSummaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell_summary', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->unsignedBigInteger('employee');
            $table->dateTime('created_date');
            $table->dateTime('last_update');
            $table->integer('price_total');
            $table->decimal('discount_total', 11, 2);
            $table->decimal('total', 11, 2);
        });
        Schema::table('sell_summary', function (Blueprint $table) {
            $table->foreign('employee')->references('id')->on('employees')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sell_summary');
    }
}
