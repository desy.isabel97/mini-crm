<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSellTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sell', function (Blueprint $table) {
            $table->id();
            $table->dateTime('created_date');
            $table->unsignedBigInteger('item');
            $table->integer('price');
            $table->decimal('discount', 5, 2);
            $table->unsignedBigInteger('employee');
        });
        Schema::table('sell', function (Blueprint $table) {
            $table->foreign('item')->references('id')->on('items')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('employee')->references('id')->on('employees')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sell');
    }
}
