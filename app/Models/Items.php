<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Sell;
use App\Models\Items;
use DB;

class Items extends Model
{
    use HasFactory;

    protected $table = "items";
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'price',
        'created_at',
        'updated_at'
    ];

    public function sells()
    {
        return $this->hasMany(Sell::class);
    }

    public static function getItems() {
        DB::statement(DB::raw('set @rownum=0'));
        $data = Items::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'name',
            'price',
            'created_at',
        ]);

        return $data;
    }
}
