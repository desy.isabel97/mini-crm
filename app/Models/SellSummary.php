<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Employees;
use Carbon\Carbon;

class SellSummary extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = "sell_summary";
    protected $primaryKey = 'id';

    protected $fillable = [
        'date',
        'employee',
        'created_date',
        'last_update',
        'price_total',
        'discount_total',
        'total'
    ];

    public function hasEmployee()
    {
        return $this->belongsTo(Employees::class, 'employee', 'id');
    }

    public static function isSellSummaryExist($date, $employee)
    {
        $dateFormat = $date->format('Y-m-d');

        $sell_summary = SellSummary::where('date', $dateFormat)->where('employee', $employee)->first();

        if($sell_summary !== null){
            return true;
        }
        else{
            return false;
        }
    }
}
