<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Items;
use App\Models\Employees;

class Sell extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = "sell";
    protected $primaryKey = 'id';

    protected $fillable = [
        'created_date',
        'item',
        'price',
        'discount',
        'employee'
    ];

    public function hasItem()
    {
        return $this->belongsTo(Items::class, 'item', 'id');
    }

    public function hasEmployee()
    {
        return $this->belongsTo(Employees::class, 'employee', 'id');
    }
}
