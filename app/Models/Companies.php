<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// use Kyslik\ColumnSortable\Sortable;
use App\Models\Employees;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;
use DB;

class Companies extends Model implements ShouldQueue
{
    use HasFactory;

    public function employees()
    {
        return $this->hasMany(Employees::class);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "companies";
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'email', 'logo', 'website', 'created_at', 'updated_at', 'created_by_id', 'updated_by_id', 'domain'];

    public static function getCompanies() {
        DB::statement(DB::raw('set @rownum=0'));
        $data = Companies::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'name',
            'email',
            'logo',
            'website',
            'created_at',
        ]);

        return $data;
    }
}
