<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Companies;
use Carbon\Carbon;
use DB;

class Employees extends Model
{
    use HasFactory;

    protected $table = "employees";
    protected $primaryKey = 'id';

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'company',
        'created_at',
        'updated_at',
        'created_by_id',
        'updated_by_id',
        'password'
    ];

    public function companyName()
    {
        return $this->belongsTo(Companies::class, 'company', 'id');
    }

    public static function getEmployees() {
        DB::statement(DB::raw('set @rownum=0'));
        $data = Employees::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'first_name',
            'last_name',
            'email',
            'phone',
            'created_at',
        ]);

        return $data;
    }
}
