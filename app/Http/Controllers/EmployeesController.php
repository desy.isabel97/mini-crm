<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employees;
use App\Models\Companies;
use App\Models\Timezone;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use DB;
use App\Http\Requests\StoreEmployeeRequest;
use App\Http\Traits\TimezoneTrait;
use App\Http\Traits\EmployeesTrait;

class EmployeesController extends Controller
{
    use TimezoneTrait;
    use EmployeesTrait;
    
    public function index(Request $request)
    {
        if(!empty($request->timezone)){
            $this->setTimezone($request->timezone);
        }

        $timezones = Timezone::Orderby('offset')->get();

        if($request->ajax()) {
            $employees_datatable = $this->setEmployeesTable();

            return $employees_datatable;
        }
        $companies = Companies::pluck('name', 'id');

        return view('employees.employees-index', [
            'companies' => $companies,
            'timezones' => $timezones,
        ]);
    }

    public function show(Employees $employee)
    {
        $company = Companies::find($employee->company);

        return view('employees.employee-detail', compact('company', 'employee'));
    }

    public function store(StoreEmployeeRequest $request)
    {
        $this->createEmployee($request);

        return response()->json(['success' => 'Data Added successfully.']);

    }

    public function edit($id)
    {
        if(request()->ajax())
        {
            $data = Employees::findOrFail($id);
            return response()->json(['result' => $data]);
        }
    }

    public function update(StoreEmployeeRequest $request, Employees $employees)
    {
        if (empty($request->password))
        {
            $employee = Employees::find($request->hidden_id);
            $this->updateEmployee($request, $employee->password);
        }
        else {            
            $this->updateEmployee($request);
        }

        return response()->json(['success' => 'Data is successfully updated']);

    }

    public function destroy($id)
    {
        $data = Employees::findOrFail($id);
        $data->delete();
    }
}
