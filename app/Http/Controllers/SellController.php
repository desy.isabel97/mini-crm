<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sell;
use App\Models\SellSummary;
use App\Models\Items;
use App\Models\Employees;
use App\Models\Timezone;
use Carbon\Carbon;
use DB;
use Yajra\Datatables\Datatables;
use App\Http\Requests\StoreSellRequest;
use Illuminate\Support\Facades\Redirect;
use App\Http\Traits\SellSummaryTrait;
use App\Http\Traits\TimezoneTrait;
use App\Http\Traits\SellTrait;

class SellController extends Controller
{
    use SellSummaryTrait;
    use TimezoneTrait;
    use SellTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!empty($request->timezone)) {
            $this->setTimezone($request->timezone);
        }

        $timezones = Timezone::Orderby('offset')->get();

        if($request->ajax()){
            $sell_datatable = $this->setSellTable();

            return $sell_datatable;
        }
        return view('sell.sell-index', compact('timezones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Items::pluck('name', 'id');
        $employees = Employees::Orderby('first_name')->get();
        return view('sell.sell-create', compact('items', 'employees'));
    }

    public function storeItemPrice($id)
    {
        $item = Items::where('id', $id)->first();
    
        return response()->json($item);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSellRequest $request)
    {
        $new_sell = $this->createSell($request);

        $check_sell_summary = SellSummary::isSellSummaryExist($new_sell->created_date, $new_sell->employee);

        if ($check_sell_summary == true) {
            $this->updateSellSummary($new_sell);
        }
        else{
            $this->storeSellSummary($new_sell);
        }

        return redirect()->route('sell.show', ['sell' => $new_sell])->with('success', 'null');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sell = Sell::where('id', $id)->first();
        $date = Carbon::parse($sell->created_at)->format('d m Y');

        return view('sell.sell-detail', compact('sell', 'date'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Sell $sell)
    {
        $items = Items::pluck('name', 'id');
        $employees = Employees::Orderby('first_name')->get();
        return view('sell.sell-edit', compact('sell', 'items', 'employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreSellRequest $request, $id)
    {
        $old_sell = Sell::find($id);
        $old_price      =   $old_sell->price;
        $old_discount   =   $old_sell->discount;
        $old_employee   =   $old_sell->employee;

        $updated_sell = Sell::find($id);
        $updated_sell->item = $request->item;
        $updated_sell->price = $request->price;
        $updated_sell->discount = $request->discount;
        $updated_sell->employee = $request->employee;
        $updated_sell->save();

        $created_date = Carbon::createFromFormat('Y-m-d H:i:s', $updated_sell->created_date);

        $check_sell_summary = SellSummary::isSellSummaryExist($created_date, $updated_sell->employee);

        if($check_sell_summary == true){
            $this->storeSellSummarybyUpdate(2, $updated_sell, $old_price, $old_discount, $old_employee);
        }
        else{
            $this->storeSellSummarybyUpdate(1, $updated_sell, $old_price, $old_discount, $old_employee);
        }

        return redirect()->route('sell.show', ['sell' => $updated_sell])->with('success', 'null');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Sell::findOrFail($id);
        $this->updateSellSummarybyDelete($data);
        $data->delete();

        return redirect()->route('sell.index');
    }
}
