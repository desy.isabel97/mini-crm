<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SellSummary;
use App\Models\Timezone;
use App\Models\Employees;
use App\Models\Companies;
use DB;
use Yajra\Datatables\Datatables;
use App\Http\Traits\SellSummaryTrait;
use App\Http\Traits\TimezoneTrait;

class SellSummaryController extends Controller
{
    use SellSummaryTrait;
    use TimezoneTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!empty($request->timezone)){
            $this->setTimezone($request->timezone);
        }

        $timezones = Timezone::Orderby('offset')->get();
        $employees = Employees::Orderby('first_name')->get();
        $companies = Companies::Orderby('name')->get();

        if($request->ajax()){
            $sell_summary_datatable = $this->setSellSummaryTable($request);

            return $sell_summary_datatable;
        }
        return view('sell-summary.sell-summary-index', compact('timezones', 'employees', 'companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $sell_summary = SellSummary::where('id', $id)->first();
        $sells = $this->getSells($id);

        return view('sell-summary.sell-summary-detail', compact('sell_summary', 'sells'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
