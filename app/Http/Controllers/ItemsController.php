<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Items;
use App\Models\Timezone;
use Yajra\Datatables\Datatables;
use DB;
use App\Http\Requests\StoreItemRequest;
use App\Http\Traits\TimezoneTrait;
use App\Http\Traits\ItemsTrait;

class ItemsController extends Controller
{
    use TimezoneTrait;
    use ItemsTrait;
    
    /**
     * Display a listing of items.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!empty($request->timezone)){
            $this->setTimezone($request->timezone);
        }

        $timezones = Timezone::Orderby('offset')->get();

        if($request->ajax()){
            $items_datatable = $this->setItemsTable();

            return $items_datatable;
        }

        return view('items.items-index', compact('timezones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreItemRequest $request)
    {
        $new_item = new Items();
        $new_item->name         =   $request->name;
        $new_item->price        =   $request->price;
        $new_item->created_at   =   now();
        $new_item->updated_at   =   now();
        $new_item->save();

        return response()->json(['success' => 'Data Added successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Items $item)
    {
        return view('items.item-detail', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(request()->ajax())
        {
            $data = Items::findOrFail($id);
            return response()->json(['result' => $data]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreItemRequest $request, Items $items)
    {
        $item = Items::find($request->hidden_id);
        $item->name         =   $request->name;
        $item->price        =   $request->price;
        $item->updated_at   =   now();
        $item->save();

        return response()->json(['success' => 'Data is successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Items::findOrFail($id);
        $data->delete();
    }
}
