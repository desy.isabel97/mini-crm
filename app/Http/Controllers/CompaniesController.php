<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\Companies;
use App\Models\Employees;
use App\Models\Timezone;
use App\Mail\NewCompanyMail;
use App\Jobs\NewCompanyMailJob;
use App\Http\Requests\StoreCompanyRequest;
use App\Http\Traits\TimezoneTrait;
use App\Http\Traits\CompaniesTrait;

class CompaniesController extends Controller
{
    use TimezoneTrait;
    use CompaniesTrait;
    
    public function index(Request $request)
    {
        if(!empty($request->timezone)){
            $this->setTimezone($request->timezone);
        }

        $timezones = Timezone::Orderby('offset')->get();

        if($request->ajax()){
            $companies_datatable = $this->setCompaniesTable();

            return $companies_datatable;
        }
        return view('companies.companies-index', compact('timezones'));
    }

    public function create()
    {
        return view('companies.company-create');
    }

    public function store(StoreCompanyRequest $request)
    {
        if ($request->hasfile('filename')) {
            $image = $this->handleUploadedImage($request);
            $this->createCompany($request, $image);
        }
        else {
            $this->createCompany($request);
        }

        //send email notification
        NewCompanyMailJob::dispatch();

        return Redirect::to('/companies')->with('success', 'null');
    }

    public function show(Companies $company)
    {
        $employees = Employees::where('company', '=', $company->id)->get();

        return view('companies.company-detail', compact('company', 'employees'));
    }

    public function edit(Companies $company)
    {
        return view('companies.company-edit', compact('company'));
    }

    public function update(StoreCompanyRequest $request, $id)
    {
        $company = Companies::find($id);

        if ($request->hasfile('filename')) {
            $image = $this->handleUploadedImage($request);
            $this->updateCompany($request, $id, $image);
            $this->deleteLogo($company->logo);
        }
        else {
            $this->updateCompany($request, $id, null, false, $company->logo);
        }

        return Redirect::route('companies.show', $id)->with('success', 'null');

    }

    public function destroy($id)
    {
        $data = Companies::findOrFail($id);
        $this->deleteLogo($data->logo);
        $data->delete();
        
        return Redirect::to('/companies');
    }
}
