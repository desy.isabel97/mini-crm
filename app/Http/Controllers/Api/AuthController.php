<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Employees;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if( !Auth::attempt($request->only('email', 'password')) ){
            return response([
                'message' => 'Invalid Credentials'
            ], Response::HTTP_UNAUTHORIZED);
        }

        $user = Auth::user();
        $token = $user->createToken('tokenUser')->plainTextToken;

        // return response([
        //     // 'message' => 'Success',
        //     'token' => $token
        // ], 200);
        return response()->json([
            'token' => $token, 
            'token_type' => 'Bearer',
        ], 200);
    }

    public function employeesByCompany(Request $request)
    {
        $request->validate([ 'company_id' => 'required', ]);
        $employees = Employees::with('companyName')->where('company', $request->company_id)->get();

        return response()->json([
            'message' => 'success',
            'data' => $employees,
            'user' => Auth::user()
        ], 200);

    }
}
