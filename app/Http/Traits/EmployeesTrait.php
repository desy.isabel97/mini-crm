<?php
namespace App\Http\Traits;

use App\Models\Employees;
use Yajra\Datatables\Datatables;

trait EmployeesTrait
{
    public function setEmployeesTable(){
        $data = Employees::getEmployees();

        return DataTables::of($data)
            ->editColumn('created_at', function ($value){
                return setNewTimezone($value->created_at);
            })
            ->addColumn('action', function($data){
                $showUrl = route('employees.show', $data->id);
                $view = trans('text.View');
                $edit = trans('text.Edit');
                $delete = trans('text.Delete');
                
                $button = '<a href=' . $showUrl . ' class="btn btn-success btn-sm">' . $view . '</a>
                ';
                $button .= '<button type="button" name="edit" id="'.$data->id.'" class="edit btn btn-primary btn-sm">' . $edit . '</button>';
                $button .= '&nbsp;<button type="button" name="edit" id="'.$data->id.'" class="delete btn btn-danger btn-sm">' . $delete . '</button>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function createEmployee($request) {
        Employees::create([
            'first_name'       =>   ucwords($request->first_name),
            'last_name'        =>   ucwords($request->last_name),
            'email'            =>   $request->email,
            'phone'            =>   $request->phone,
            'company'          =>   $request->company,
            'password'         =>   bcrypt($request->password),
            'created_at'       =>   now(),
            'updated_at'       =>   now(),
            'created_by_id'    =>   session('token')->id,
            'updated_by_id'    =>   session('token')->id,
        ]);
    }

    public function updateEmployee($request, $old_password = null) {
        Employees::where('id', $request->hidden_id)->update([
            'first_name'       =>   ucwords($request->first_name),
            'last_name'        =>   ucwords($request->last_name),
            'email'            =>   $request->email,
            'phone'            =>   $request->phone,
            'company'          =>   $request->company,
            'password'         =>   ($request->password) ? bcrypt($request->password) : $old_password,
            'created_at'       =>   now(),
            'updated_at'       =>   now(),
            'created_by_id'    =>   session('token')->id,
            'updated_by_id'    =>   session('token')->id,
        ]);
    }
}