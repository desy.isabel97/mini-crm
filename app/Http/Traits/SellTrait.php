<?php
namespace App\Http\Traits;

use App\Models\Sell;
use App\Models\SellSummary;
use Yajra\Datatables\Datatables;
use DB;

trait SellTrait
{
    public function getSell() {
        DB::statement(DB::raw('set @rownum=0'));
        $data = Sell::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'sell.id',
            'sell.created_date',
            'sell.item',
            'sell.price',
            'sell.discount',
            'sell.employee',
        ])->with('hasItem', 'hasEmployee');

        return $data;
    }

    public function setSellTable(){
        $data = $this->getSell();
        
        return DataTables::of($data)
            ->editColumn('created_date', function ($value){
                return setNewTimezone($value->created_date);
            })
            ->editColumn('item', function ($data){
                return $data->hasItem->name;
            })
            ->editColumn('employee', function ($value){
                return $value->hasEmployee->first_name . " " . $value->hasEmployee->last_name;
            })
            ->addColumn('action', function($data){
                $showUrl = route('sell.show', $data->id);
                $editUrl = route('sell.edit', $data->id);
                $view = trans('text.View');
                $edit = trans('text.Edit');
                $delete = trans('text.Delete');

                $button = '<a href=' . $showUrl . ' class="btn btn-success btn-sm">' . $view . '</a>
                ';
                $button .= '<a href='. $editUrl . ' class="btn btn-primary btn-sm">' . $edit . '</a>
                ';
                $button .= '<button type="button" name="edit" id="'.$data->id.'" class="delete btn btn-danger btn-sm">' . $delete . '</button>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function createSell($request) {
        return Sell::create([
            'created_date'  =>   now(),
            'item'          =>   $request->item,
            'price'         =>   $request->price,
            'discount'      =>   $request->discount,
            'employee'      =>   $request->employee,
        ]);
    }
}