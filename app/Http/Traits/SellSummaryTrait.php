<?php
namespace App\Http\Traits;

use App\Models\Sell;
use App\Models\SellSummary;
use App\Models\Employees;
use App\Http\Requests\StoreSellRequest;
use Carbon\Carbon;
use DB;
use Yajra\Datatables\Datatables;

trait SellSummaryTrait
{
    public function setSellSummaryTable($request){
        
        $data = $this->filterSellSummary($request);

        return DataTables::of($data)
            ->editColumn('created_date', function ($data){
                return setNewTimezone($data->created_date);
            })
            ->editColumn('employee', function ($data){
                return $data->hasEmployee->first_name . " " . $data->hasEmployee->last_name;
            })
            ->addColumn('company', function($data){
                return $data->hasEmployee->companyName->name;
            })
            ->addColumn('action', function($data){
                $showUrl = route('sell-summary.show', $data->id);
                $view = trans('text.View');

                $button = '<a href=' . $showUrl . ' class="btn btn-success btn-sm">' . $view . '</a>';
                return $button;
            })
            ->rawColumns(['action'])
            ->make(true);
    }    

    public function storeSellSummary($sell){
        $discount = $sell->price * $sell->discount;
        $final_price = $sell->price - $discount;

        $sell_summary = new SellSummary();
        $sell_summary->date = $sell->created_date->format('Y-m-d');
        $sell_summary->employee = $sell->employee;
        $sell_summary->created_date = now();
        $sell_summary->last_update = now();
        $sell_summary->price_total = $sell->price;
        $sell_summary->discount_total = $discount;
        $sell_summary->total = $final_price;

        $sell_summary->save();
    }

    public function updateSellSummary($sell){
        $discount = $sell->price * $sell->discount;
        $final_price = $sell->price - $discount;

        $created_date = Carbon::createFromFormat('Y-m-d H:i:s', $sell->created_date);
        $dateFormat = $created_date->format('Y-m-d');
        $sell_summary = SellSummary::where('date', $dateFormat)->where('employee', $sell->employee)->first();

        $sell_summary->last_update = now();
        $sell_summary->price_total += $sell->price;
        $sell_summary->discount_total += $discount;
        $sell_summary->total += $final_price;

        $sell_summary->save();
    }

    public function storeSellSummarybyUpdate($key, $updated_sell, $old_price, $old_discount, $old_employee){
        $discount_old = $old_price * $old_discount;
        $final_price_old = $old_price - $discount_old;

        $created_date = Carbon::createFromFormat('Y-m-d H:i:s', $updated_sell->created_date);
        $dateFormat = $created_date->format('Y-m-d');

        $old_sell_summary = SellSummary::whereDate('date', $dateFormat)->where('employee', $old_employee)->first();
        $old_sell_summary->last_update = now();
        $old_sell_summary->price_total -= $old_price;
        $old_sell_summary->discount_total -= $discount_old;
        $old_sell_summary->total -= $final_price_old;
        $old_sell_summary->save();

        if ($key == 1) { //new SellSummary
            $this->storeSellSummary($updated_sell);
        }
        else if ($key == 2) { //Update SellSummary
            $this->updateSellSummary($updated_sell);
        }            
    }

    public function updateSellSummarybyDelete($sell){
        $discount = $sell->price * $sell->discount;
        $final_price = $sell->price - $discount;

        $dateFormat = Carbon::parse($sell->created_date)->format('Y-m-d');
        $sell_summary = SellSummary::where('date', $dateFormat)->where('employee', $sell->employee)->first();

        $sell_summary->last_update = now();
        $sell_summary->price_total -= $sell->price;
        $sell_summary->discount_total -= $discount;
        $sell_summary->total -= $final_price;

        $sell_summary->save();
    }

    public function getSells($id){
        $sell_summary = SellSummary::where('id', $id)->first();
        $sells = Sell::whereDate('created_date', $sell_summary->date)
                        ->where('employee', $sell_summary->employee)
                        ->with('hasEmployee', 'hasItem')
                        ->get();
        return $sells;
    }

    public function filterSellSummary($request) {
        DB::statement(DB::raw('set @rownum=0'));
        
        if (!empty($request->from_date)) {
            if (!empty($request->employee)) {
                if (!empty($request->company)) {
                    return $this->filterDateEmployeeCompany($request->from_date, $request->to_date, $request->employee, $request->company);
                }
                else {
                    return $this->filterDateEmployee($request->from_date, $request->to_date, $request->employee);
                }
            }
            else {
                if (!empty($request->company)) {
                    return $this->filterDateCompany($request->from_date, $request->to_date, $request->company);
                }
                else {
                    return $this->filterDate($request->from_date, $request->to_date);
                }
            }                
        }
        else {
            if (!empty($request->employee)) {
                if (!empty($request->company)) {
                    return $this->filterEmployeeCompany($request->employee, $request->company);
                }
                else {
                    return $this->filterEmployee($request->employee);
                }
            }
            else {
                if (!empty($request->company)) {
                    return $this->filterCompany($request->company);
                    // return $data;
                }
                else {
                    return $this->dataSellSummary();
                }
            }
        }
    }

    public function dataSellSummary() {
        return SellSummary::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'date',
            'employee',
            'created_date',
            'price_total',
            'discount_total',
            'total',
        ]);
    }

    public function filterDate($from_date, $to_date){
        return $this->dataSellSummary()
                    ->whereBetween('date', array($from_date, $to_date));
    }

    public function filterDateEmployee($from_date, $to_date, $employee){
        return $this->dataSellSummary()
                    ->whereBetween('date', array($from_date, $to_date))
                    ->where('employee', $employee);
    }

    public function filterDateCompany($from_date, $to_date, $company){
        $employees = Employees::select('id')->where('company', $company)->get();

        return $this->dataSellSummary()
                    ->whereIn('employee', $employees)
                    ->whereBetween('date', array($from_date, $to_date));
    }

    public function filterDateEmployeeCompany($from_date, $to_date, $employee, $company){
        $employees = Employees::select('id')->where('company', $company)->get();

        return $this->dataSellSummary()
                    ->where('employee', $employee)
                    ->whereIn('employee', $employees)
                    ->whereBetween('date', array($from_date, $to_date));
    }

    public function filterEmployee($employee){
        return $this->dataSellSummary()
                    ->where('employee', $employee);
    }

    public function filterEmployeeCompany($employee, $company){
        $employees = Employees::select('id')->where('company', $company)->get();

        return $this->dataSellSummary()
                    ->where('employee', $employee)
                    ->whereIn('employee', $employees);
    }

    public function filterCompany($company){
        $employees = Employees::select('id')->where('company', $company)->get();

        return $this->dataSellSummary()
                    ->whereIn('employee', $employees);
    }
}