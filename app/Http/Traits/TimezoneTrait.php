<?php
namespace App\Http\Traits;

use App\Models\Timezone;

trait TimezoneTrait
{
    public function setTimezone($timezone_id){
        $timezone = Timezone::find($timezone_id);
        session(['timezone_id' => $timezone_id ]);
        session(['timezone' => $timezone->name ]);
    }

    
}