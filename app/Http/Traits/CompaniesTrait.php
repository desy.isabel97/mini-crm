<?php
namespace App\Http\Traits;

use App\Models\Companies;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;

trait CompaniesTrait
{
    public function setCompaniesTable(){
        $data = Companies::getCompanies();
        
        return DataTables::of($data)
            ->editColumn('created_at', function ($value){
                return setNewTimezone($value->created_at);
            })
        ->addColumn('image', function ($data) {
            $url= asset('storage/'.$data->logo);
            return $url;
        })
        ->addColumn('action', function($data){
            $showUrl = route('companies.show', $data->id);
            $editUrl = route('companies.edit', $data->id);
            $view = trans('text.View');
            $edit = trans('text.Edit');
            $delete = trans('text.Delete');

            $button = '<a href=' . $showUrl . ' class="btn btn-success btn-sm">' . $view . '</a>
            ';
            $button .= '<a href=' . $editUrl . ' class="btn btn-primary btn-sm">' . $edit . '</a>
            ';
            $button .= '<button type="button" name="edit" id="'.$data->id.'" class="delete btn btn-danger btn-sm">' . $delete . '</button>';
            return $button;
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    public function handleUploadedImage($request) {
        $image = $request->file('filename');
        $path = Storage::putFile('public', $image);

        return $path;
    }

    public function createCompany($request, $image = null) {
        Companies::create([
            'name' => ucwords($request->name),
            'email' => $request->email,
            'logo' => ($image) ? $image : "",
            'website' => $request->website,
            'created_at' => now(),
            'updated_at' => now(),
            'created_by_id' => session('token')->id,
            'updated_by_id' => session('token')->id,
            'domain' => $request->domain,
        ]);
    }

    public function updateCompany($request, $id, $new_logo = null, $is_new_logo = true, $old_logo = null) {
        Companies::where('id', $id)
            ->update([
                'name' => $request->name,
                'email' => $request->email,
                'logo' => ($is_new_logo) ? $new_logo : $old_logo,
                'website' => $request->website,
                'domain' => $request->domain,
                'updated_at' => now(),
                'updated_by_id' => session('token')->id
            ]);
    }

    public function deleteLogo($logo) {
        Storage::disk('public')->delete($logo);
    }
}