<?php
namespace App\Http\Traits;

use App\Models\Items;
use Yajra\Datatables\Datatables;

trait ItemsTrait
{
    public function setItemsTable(){
        $data = Items::getItems();
        
        return DataTables::of($data)
            ->editColumn('created_at', function ($value){
                return setNewTimezone($value->created_at);
            })
        ->addColumn('action', function($data){
            $showUrl = route('items.show', $data->id);
            $view = trans('text.View');
            $edit = trans('text.Edit');
            $delete = trans('text.Delete');

            $button = '<a href=' . $showUrl . ' class="btn btn-success btn-sm">' . $view . '</a>
            ';
            $button .= '<button type="button" name="edit" id="'.$data->id.'" class="edit btn btn-primary btn-sm">' . $edit . '</button>
            ';
            $button .= '<button type="button" name="edit" id="'.$data->id.'" class="delete btn btn-danger btn-sm">' . $delete . '</button>';
            return $button;
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    
}