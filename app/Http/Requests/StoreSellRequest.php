<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSellRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item'          => 'required',
            'price'         => 'required',
            'discount'      => 'required',
            'employee'      => 'required',
        ];
    }

    public function messages()
    {
        // if ($this->getLanguage() == "en") 
        // {
            return [
                'item.required' => 'Item Name is required',
                'price.required' => 'Price is required',
                'employee.required' => 'Employee is required',
                'discount.required' => 'Discount is required',
        //     ];
        // }else{
        //     return [
        //         'item_id.required' => 'Harap pilih item',
        //         'price.required' => 'Harap isi harga',
        //         'id_employee.required' => 'Harap pilih karyawan',
        //         'discount.required' => 'Harap isi diskon',
            ];
        // }
    }

}
