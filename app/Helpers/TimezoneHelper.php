<?php

use Carbon\Carbon;

    function setNewTimezone($value){
        $string_date = date('Y-m-d H:i:s', strtotime($value));
        $created_at = Carbon::createFromFormat('Y-m-d H:i:s', $string_date, 'UTC');
        $created_at->setTimezone(\Session::get('timezone'));
        return $created_at->toDateTimeString();
    }