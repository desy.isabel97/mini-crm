<?php

// return [

//     /*
//     |--------------------------------------------------------------------------
//     | Authentication Language Lines
//     |--------------------------------------------------------------------------
//     |
//     | The following language lines are used during authentication for various
//     | messages that we need to display to the user. You are free to modify
//     | these language lines according to your application's requirements.
//     |
//     */

//     'welcome' => 'Welcome to our application!',
//     'Companies'=>'Companies',
//     'Employees'=>'Employees',
//     'Companies List'=>'Companies List',
//     'Employees List'=>'Employees List',
//     'Company'=>'Company',
//     'Employee'=>'Employee',
//     'Login'=>'Login',
//     'Logout'=>'Logout',
//     'Name'=>'Name',
//     'First Name'=>'First Name',
//     'Last Name'=>'Last Name',
//     'Email'=>'Email',
//     'Phone'=>'Phone',
//     'Website'=>'Website',
//     'Logo'=>'Logo',
//     'View'=>'View',
//     'Detail'=>'Detail',
//     'Add'=>'Add',
//     'Edit'=>'Edit',
//     'Delete'=>'Delete',
//     'Action'=>'Action',
//     'Confirmation'=>'Confirmation',
//     'Are you sure you want to remove this data?'=>'Are you sure you want to remove this data?',
//     'Cancel'=>'Cancel',
//     'Max. Size'=>'Max. Size',
//     'Min. Dimension'=>'Min. Dimension',
//     'CreatedAt'=>'Created Date',

// ];
