<?php

// return [

//     /*
//     |--------------------------------------------------------------------------
//     | Authentication Language Lines
//     |--------------------------------------------------------------------------
//     |
//     | The following language lines are used during authentication for various
//     | messages that we need to display to the user. You are free to modify
//     | these language lines according to your application's requirements.
//     |
//     */

//     'welcome' => 'Selamat Datang!',
//     'Companies'=>'perusahaan',
//     'Employees'=>'pegawai',
//     'Companies List'=>'Daftar Perusahaan',
//     'Employees List'=>'Daftar Pegawai',
//     'Company'=>'Perusahaan',
//     'Employee'=>'Pegawai',
//     'Login'=>'Masuk',
//     'Logout'=>'Keluar',
//     'Name'=>'Nama',
//     'First Name'=>'Nama Awal',
//     'Last Name'=>'Nama Akhir',
//     'Email'=>'Alamat Surel',
//     'Phone'=>'No. Telepon',
//     'Website'=>'Situs web',
//     'Logo'=>'Logo',
//     'View'=>'lihat',
//     'Detail'=>'Detil',
//     'add'=>'Tambah',
//     'Edit'=>'Ubah',
//     'Delete'=>'Hapus',
//     'Action'=>'Aksi',
//     'Confirmation'=>'Konfirmasi',
//     'Are you sure you want to remove this data?'=>'Anda yakin ingin menghapus data?',
//     'Cancel'=>'Batal',
//     'Max. Size'=>'Ukuran Maksimal',
//     'Min. Dimension'=>'Dimensi Minimal',
//     'CreatedAt'=>'Tanggal Ditambahkan',

// ];
