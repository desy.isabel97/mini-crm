<!-- <link href="{{ asset('css/style.css') }}" rel="stylesheet"> -->
@extends('adminlte::page')

@section('title', trans('text.Edit') .  trans('text.Company'))

@section('content_header')
    <h1>{{ trans('text.Edit') }} {{ trans('text.Company') }}</h1>
@stop

@section('content')
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <div class="container">
        <div class="row background">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <form method="post" action="{{ route('companies.update', $company->id) }}" enctype="multipart/form-data">
                            
                            @csrf
                            @method('PATCH')
                            <div class="row mt-2">
                                <div class="col-md-4 text-left card-caption-home">{{ trans('text.Name') }}</div>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="name" value="{{ $company->name }}"/>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-4 text-left card-caption-home">{{ trans('text.Email') }}</div>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="email" value="{{ $company->email }}"/>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-4 text-left card-caption-home">{{ trans('text.Website') }}</div>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="website" value="{{ $company->website }}"/>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-4 text-left card-caption-home">{{ trans('text.Domain') }}</div>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="domain" value="{{ $company->domain }}"/>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-4 text-left card-caption-home">{{ trans('text.Logo') }}</div>
                                <div class="col-md-8">                                            
                                    <p>
                                        <img src="{{ asset('storage/'.$company->logo)}}" style="width:200px; object-fit: cover;"/>
                                    </p>
                                    <input type="file" name="filename">
                                    <br/><label>{{ trans('text.maxSize') }}: 2MB <br/> {{ trans('text.minDim') }}: 100 x 100 px</label>
                                </div>
                                <div class="col-md-4 text-left  card-caption-home"></div>
                                <div class="col-md-8">
                                    <button type="submit" class="btn btn-primary" style="margin-top:12px">{{ trans('text.Edit') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop


@section('js')
    <script> console.log('Hi!'); </script>
@stop