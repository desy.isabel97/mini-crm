<!-- <link href="{{ asset('css/style.css') }}" rel="stylesheet"> -->
@extends('adminlte::page')

@section('title', trans('text.AddCompany'))

@section('content_header')
    <h1>{{ trans('text.AddCompany') }}</h1>
@stop

@section('content')
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <div class="container">
    <div class="row background">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('companies.store')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row mt-2">
                            <div class="col-md-2 text-left card-caption-home">{{ trans('text.Name') }}</div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="name"/>
                            </div>
                        </div>
                        
                        <div class="row mt-2">
                            <div class="col-md-2 text-left card-caption-home">{{ trans('text.Email') }}</div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="email" />
                            </div>
                        </div>
                        
                        <div class="row mt-2">
                            <div class="col-md-2 text-left card-caption-home">{{ trans('text.Website') }}</div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="website" />
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2 text-left card-caption-home">{{ trans('text.Domain') }}</div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="domain" />
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2 text-left card-caption-home">{{ trans('text.Logo') }}</div>
                            <div class="col-md-8">
                                <input type="file" name="filename" class="form-control">
                                <label>{{ trans('text.maxSize') }}: 2MB <br/> {{ trans('text.minDim') }}: 100 x 100 px</label>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-2 text-left"></div>
                            <div class="col-md-8">
                                <button type="submit" class="btn btn-success" style="margin-top:12px"><i class="glyphicon glyphicon-check"></i>{{ trans('text.Add') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop