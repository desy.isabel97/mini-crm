@extends('adminlte::page')

@section('title', trans('text.Company Detail'))

@section('content_header')
    <h1>{{ trans('text.Company Detail') }}</h1>
@stop

@section('content')<!doctype html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <a href="{{route('companies.edit', $company->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i> {{ trans('text.Edit') }}</a>
    <button class="btn btn-danger" data-toggle="modal" data-target="#Modal"><i class="fa fa-trash"></i> {{ trans('text.Delete') }}</button>
    <div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" align="left">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="sub-title" id="exampleModalLabel">{{ trans('text.Confirmation') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{ trans('text.deleteConf') }}
                </div>
                <div class="modal-footer">
                    <a href="{{ URL('/company/destroy/'.$company->id) }}" type="button" class="btn btn-danger">OK</a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('text.Cancel') }}</button>
                </div>
            </div>
        </div>
    </div>

    <table style="width:100%">
        <tr>
            <th>{{ trans('text.Name') }}</th>
            <td>{{$company->name}}</td>
        </tr>
        <tr>
            <th>{{ trans('text.Email') }}</th>
            <td>{{$company->email}}</td>
        </tr>
        <tr>
            <th>{{ trans('text.Website') }}</th>
            <td>{{$company->website}}</td>
        </tr>
        <tr>
            <th>{{ trans('text.Domain') }}</th>
            <td>{{$company->domain}}</td>
        </tr>
        <tr>
            <th>{{ trans('text.Logo') }}</th>
            <td><img src="{{ asset('storage/'.$company->logo) }}" style="height: 200px" /></td>
        </tr>
        <tr>
            <th>{{ trans('text.Employees') }}</th>
            <td>
                <?php foreach ($employees as $employee) { ?>
                    <a href="{{route('employees.show', $employee->id)}}"> {{ $employee->first_name }} {{ $employee->last_name }} <br/></a>
                <?php } ?>
            </td>
        </tr>
    </table>
@stop

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
@stop