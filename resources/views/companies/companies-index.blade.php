@extends('adminlte::page')

@section('title', trans('text.Companies List'))

@section('content_header')
    <h1>{{ trans('text.Companies List') }}</h1>
@stop

@section('content')<!doctype html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <hr>
    <div class="container">
        <a href="{{ route('companies.create')}}" class="btn btn-success">{{ trans('text.Add') }} {{ trans('text.Company') }}</a>
    </div>
    <br/>

    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <select data-column="5" class="form-control filter-select" name="timezone" id="timezone">
                    <option value="">{{ trans('text.Timezone') }}...</option>
                    @foreach($timezones as $timezone)
                        @if(session('timezone_id') == $timezone->id)
                            <option value="{{ $timezone->id }}" selected>{{ $timezone->name }} ({{ $timezone->offset }})</option>
                        @else
                            <option value="{{ $timezone->id }}">{{ $timezone->name }} ({{ $timezone->offset }})</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="col-md-1">
                <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
            </div>
        </div>
        <table class="table table-bordered" id="company_table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>{{ trans('text.Logo') }}</th>
                    <th>{{ trans('text.Name') }}</th>
                    <th>{{ trans('text.Email') }}</th>
                    <th>{{ trans('text.Website') }}</th>
                    <th>{{ trans('text.createdAt') }}</th>
                    <th width="40%">{{ trans('text.Action') }}</th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <input type="text" data-column="2" placeholder="Name...." class="form-control filter-input">
                    </td>
                    <td>
                        <input type="text" data-column="3" placeholder="Email...." class="form-control filter-input">
                    </td>
                    <td>
                        <input type="text" data-column="4" placeholder="Website...." class="form-control filter-input">
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <div id="confirmModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">{{ trans('text.Confirmation') }}</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <p align="center" style="margin:0;">{{ trans('text.deleteConf') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('text.Cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function(){

            load_data();

            function load_data(timezone = ''){
                var t = $('#company_table').DataTable({
                    processing: true,
                    serverSide: true,
                    bLengthChange: false,
                    ajax: {
                        url: "{{ route('companies.index') }}",
                        data: {timezone:timezone}
                    },
                    columns: [
                        {
                            data: 'rownum',
                            name: 'rownum'
                        },
                        {
                            data: 'image',
                            name: 'image',
                            render: function( data, type, full, meta ) {
                                return "<img src=\"" + data + "\" style=\"width:200px;object-fit: cover;\"/>";
                            }
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'website',
                            name: 'website'
                        },
                        {
                            data: 'created_at',
                            name: 'created_at',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false
                        }
                    ],
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": [0, 1]
                    } ],
                    "order": [[ 2, 'asc' ]]
                });

                $('.filter-input').keyup(function(){
                    t.column( $(this).data('column'))
                        .search($(this).val())
                        .draw();
                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();
            };

            $('#filter').click(function() {
                var timezone = $('#timezone').val();
                if(timezone != ''){
                    $('#company_table').DataTable().destroy();
                    load_data(timezone);
                };
            });

            var company_id;

            $(document).on('click', '.delete', function(){
                company_id = $(this).attr('id');
                $('#confirmModal').modal('show');
            });

            $('#ok_button').click(function(){
                $.ajax({
                    url:"company/destroy/"+company_id,
                    success:function(data) {
                        setTimeout(function(){
                            $('#confirmModal').modal('hide');
                            $('#company_table').DataTable().ajax.reload();
                            alert('Data Deleted');
                        }, 2000);
                    }
                })
            });
        });
    </script>
@stop