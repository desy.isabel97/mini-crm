@extends('adminlte::page')

@section('title', trans('text.Employees List'))

@section('content_header')
    <h1>{{ trans('text.Employees List') }}</h1>
@stop

@section('content')<!doctype html>
    <hr>
    <div class="container">
        <button type="button" name="create_record" id="create_record" class="btn btn-success" align="left"> {{ trans('text.Add') }} {{ trans('text.Employee') }}</button>
    </div>
    <br/>

    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <select data-column="5" class="form-control filter-select" name="timezone" id="timezone">
                    <option value="">{{ trans('text.Timezone') }}...</option>
                    @foreach($timezones as $timezone)
                        @if(session('timezone_id') == $timezone->id)
                            <option value="{{ $timezone->id }}" selected>{{ $timezone->name }} ({{ $timezone->offset }})</option>
                        @else
                            <option value="{{ $timezone->id }}">{{ $timezone->name }} ({{ $timezone->offset }})</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="col-md-1">
                <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
            </div>
        </div>
        <table class="table table-bordered" id="employee_table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>{{trans('text.First Name')}}</th>
                    <th>{{ trans('text.Last Name') }}</th>
                    <th>{{ trans('text.Email') }}</th>
                    <th>{{ trans('text.Phone') }}</th>
                    <th>{{ trans('text.createdAt') }}</th>
                    <th width="25%">{{ trans('text.Action') }}</th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <input type="text" data-column="1" placeholder="{{trans('text.First Name')}}...." class="form-control filter-input">
                    </td>
                    <td>
                        <input type="text" data-column="2" placeholder="{{ trans('text.Last Name') }}...." class="form-control filter-input">
                    </td>
                    <td>
                        <input type="text" data-column="3" placeholder="{{ trans('text.Email') }}...." class="form-control filter-input">
                    </td>
                    <td>
                        <input type="text" data-column="4" placeholder="{{ trans('text.Phone') }}...." class="form-control filter-input">
                    </td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <div id="formModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"> {{ trans('text.Add') }} {{ trans('text.Employee') }}</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <form method="post" id="form_employee" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="control-label col-md-4" >{{ trans('text.First Name') }} : </label>
                            <div class="col-md-12">
                                <input type="text" name="first_name" id="first_name" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">{{ trans('text.Last Name') }} : </label>
                            <div class="col-md-12">
                                <input type="text" name="last_name" id="last_name" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">{{ trans('text.Email') }} : </label>
                            <div class="col-md-12">
                                <input type="text" name="email" id="email" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-8" id="passwordLabel"></label>
                            <div class="col-md-12">
                                <input type="text" name="password" id="password" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">{{ trans('text.Phone') }} : </label>
                            <div class="col-md-12">
                                <input type="text" name="phone" id="phone" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">{{ trans('text.Company') }} : </label>
                            <div class="col-md-12">
                                <select name="company" id="company" class="form-control">
                                    <option value="">{{ trans('text.Companies List') }}</option>
                                    @foreach ($companies as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br />
                        <div class="form-group" align="center">
                            <input type="hidden" name="action" id="action" value="Add" />
                            <input type="hidden" name="hidden_id" id="hidden_id" />
                            <input type="submit" name="action_button" id="action_button" class="btn btn-success" value="Add Company" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="confirmModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>{{ trans('text.Confirmation') }}</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <p align="center" style="margin:0;">{{ trans('text.deleteConf') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('text.Cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function(){

            load_data();

            function load_data(timezone = ''){
                var t = $('#employee_table').DataTable({
                    processing: true,
                    serverSide: true,
                    bLengthChange: false,
                    ajax: {
                        url: "{{ route('employees.index') }}",
                        data: {timezone:timezone}
                    },
                    columns: [
                        {
                            data: 'rownum',
                            name: 'rownum'
                        },
                        {
                            data: 'first_name',
                            name: 'first_name'
                        },
                        {
                            data: 'last_name',
                            name: 'last_name'
                        },
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'phone',
                            name: 'phone'
                        },
                        {
                            data: 'created_at',
                            name: 'created_at'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false
                        }
                    ],
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]]
                });

                $('.filter-input').keyup(function(){
                    t.column( $(this).data('column'))
                        .search($(this).val())
                        .draw();
                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();
            };

            $('#filter').click(function() {
                var timezone = $('#timezone').val();
                if(timezone != ''){
                    $('#employee_table').DataTable().destroy();
                    load_data(timezone);
                };
            });  

            $('#create_record').click(function(){
                $('.modal-title').text('{{trans("text.Add")}} {{trans("text.Employee")}}');
                $('#action_button').val('{{trans("text.Add")}}');
                $('#action').val('Add');
                $('#form_result').html('');
                $('#formModal').modal('show');
                document.getElementById('passwordLabel').innerHTML ='{{trans("text.Password")}} :';
                $('#first_name').val(null);
                $('#last_name').val(null);
                $('#email').val(null);
                $('#phone').val(null);
                $('#company').val(null);
            });

            $('#form_employee').on('submit', function(event){
                event.preventDefault();
                var action_url = '';
                var action_method = '';

                if($('#action').val() == 'Add') {
                    action_url = "{{ route('employees.store') }}";
                    action_method = "POST";
                }

                if($('#action').val() == 'Edit') {
                    action_url = "{{ route('employees.update', ':hidden_id') }}";
                    action_method = "PUT";
                }


                $.ajax({
                    url: action_url,
                    method:action_method,
                    data:$(this).serialize(),
                    dataType:"json",
                    success:function(data) {
                        var html = '';
                        if(data.errors) {
                            html = '<div class="alert alert-danger">';
                            for(var count = 0; count < data.errors.length; count++) {
                                html += '<p>' + data.errors[count] + '</p>';
                            }
                            html += '</div>';
                        }
                        if(data.success) {
                            html = '<div class="alert alert-success">' + data.success + '</div>';
                            $('#form_employee')[0].reset();
                            $('#employee_table').DataTable().ajax.reload();
                        }
                        $('#form_result').html(html);
                    }
                });
            });

            $(document).on('click', '.edit', function(){
                var id = $(this).attr('id');
                $('#form_result').html('');
                $.ajax({
                    url :"/employees/"+id+"/edit",
                    dataType:"json",
                    success:function(data) {
                        document.getElementById('passwordLabel').innerHTML ='{{trans("text.NewPassword")}} ({{trans("text.optional")}}): ';

                        $('#first_name').val(data.result.first_name);
                        $('#last_name').val(data.result.last_name);
                        $('#email').val(data.result.email);
                        $('#phone').val(data.result.phone);
                        $('#company').val(data.result.company);
                        $('#hidden_id').val(id);
                        $('.modal-title').text('{{trans("text.Edit")}} {{trans("text.Employee")}}');
                        $('#action_button').val('{{trans("text.Edit")}}');
                        $('#action').val('Edit');
                        $('#formModal').modal('show');
                    }
                })
            });

            var employee_id;

            $(document).on('click', '.delete', function(){
                employee_id = $(this).attr('id');
                $('#confirmModal').modal('show');
            });

            $('#ok_button').click(function(){
                $.ajax({
                    url:"employee/destroy/"+employee_id,
                    success:function(data) {
                        setTimeout(function(){
                            $('#confirmModal').modal('hide');
                            $('#employee_table').DataTable().ajax.reload();
                            alert('Data Deleted');
                        }, 2000);
                    }
                })
            });
        });
</script>
@stop