<!-- <link href="{{ asset('css/style.css') }}" rel="stylesheet"> -->
@extends('adminlte::page')

@section('title', trans('text.EditSell'))

@section('content_header')
    <h1>{{ trans('text.EditSell') }}</h1>
@stop

@section('content')
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <div class="container">
    <div class="row background">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('sell.update', $sell->id) }}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        @method('PATCH')
                        <div class="row mt-2">
                            <div class="col-md-2 text-left card-caption-home">{{ trans('text.Item') }} : </div>
                            <div class="col-md-8">
                                <select name="item" id="item" class="form-control">
                                    <option value="">{{ trans('text.Items List') }}</option>
                                    @foreach ($items as $id => $name)
                                        @if($sell->item == $id)
                                            <option value="{{ $id }}" selected>{{ $name }}</option>
                                        @else
                                            <option value="{{ $id }}">{{ $name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="row mt-2">
                            <div class="col-md-2 text-left card-caption-home">{{ trans('text.Price') }} : </div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="price" id="price" value="{{ $sell->price }}"/>
                            </div>
                        </div>
                        
                        <div class="row mt-2">
                            <div class="col-md-2 text-left card-caption-home">{{ trans('text.Discount') }} : </div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="discount" value="{{ $sell->discount }}"/>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2 text-left card-caption-home">{{ trans('text.Employee') }} : </div>
                            <div class="col-md-8">
                                <select name="employee" id="employee" class="form-control">
                                    <option value="">{{ trans('text.Employees List') }}</option>
                                    @foreach ($employees as $employee)
                                        @if($sell->employee == $employee->id)
                                            <option value="{{ $employee->id }}" selected>{{ $employee->first_name }} {{ $employee->last_name }}</option>
                                        @else
                                            <option value="{{ $employee->id }}">{{ $employee->first_name }} {{ $employee->last_name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-2 text-left"></div>
                            <div class="col-md-8">
                                <button type="submit" class="btn btn-success" style="margin-top:12px"><i class="glyphicon glyphicon-check"></i>{{ trans('text.Edit') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop


@section('js')
    <script> console.log('Hi!'); </script>
    <script>
        $(function () {
            $('#item').on('change', function () {
                var id = $(this).val();
                $.ajax({
                    url: "/store-item-price/"+id,
                    // data: {id: $(this).val()},
                    success: function (response) {
                        $('#price').val(response.price);
                    }
                })
            });
        });
    </script>
@stop