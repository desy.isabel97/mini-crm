<!-- <link href="{{ asset('css/style.css') }}" rel="stylesheet"> -->
@extends('adminlte::page')

@section('title', trans('text.AddSell'))

@section('content_header')
    <h1>{{ trans('text.AddSell') }}</h1>
@stop

@section('content')
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <div class="container">
    <div class="row background">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('sell.store')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row mt-2">
                            <div class="col-md-2 text-left card-caption-home">{{ trans('text.ItemName') }} : </div>
                            <div class="col-md-8">
                                <select name="item" id="item" class="form-control">
                                    <option value="">{{ trans('text.Items List') }}</option>
                                    @foreach ($items as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="row mt-2">
                            <div class="col-md-2 text-left card-caption-home">{{ trans('text.Price') }} : </div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="price" id="price" />
                            </div>
                        </div>
                        
                        <div class="row mt-2">
                            <div class="col-md-2 text-left card-caption-home">{{ trans('text.Discount') }} : </div>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="discount" />
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2 text-left card-caption-home">{{ trans('text.Employee') }} : </div>
                            <div class="col-md-8">
                                <select name="employee" id="employee" class="form-control">
                                    <option value="">{{ trans('text.Employees List') }}</option>
                                    @foreach ($employees as $employee)
                                        <option value="{{ $employee->id }}">{{ $employee->first_name }} {{ $employee->last_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-2 text-left"></div>
                            <div class="col-md-8">
                                <button type="submit" class="btn btn-success" style="margin-top:12px"><i class="glyphicon glyphicon-check"></i>{{ trans('text.Add') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop


@section('js')
    <script> console.log('Hi!'); </script>
    <script>
        $(function () {
            $('#item').on('change', function () {
                var id = $(this).val();
                $.ajax({
                    url: "/store-item-price/"+id,
                    // data: {id: $(this).val()},
                    success: function (response) {
                        $('#price').val(response.price);
                    }
                })
            });
        });
    </script>
@stop