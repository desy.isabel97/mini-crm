@extends('adminlte::page')

@section('title', trans('text.Items List'))

@section('content_header')
    <h1>{{ trans('text.Items List') }}</h1>
@stop

@section('content')<!doctype html>
    <hr>
    <div class="container">
        <button type="button" name="create_record" id="create_record" class="btn btn-success" align="left"> {{ trans('text.Add') }} {{ trans('text.Item') }}</button>
    </div>
    <br/>

    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <select data-column="5" class="form-control filter-select" name="timezone" id="timezone">
                    <option value="">{{ trans('text.Timezone') }}...</option>
                    @foreach($timezones as $timezone)
                        @if(session::get('timezone_id') == $timezone->id)
                            <option value="{{ $timezone->id }}" selected>{{ $timezone->name }} ({{ $timezone->offset }})</option>
                        @else
                            <option value="{{ $timezone->id }}">{{ $timezone->name }} ({{ $timezone->offset }})</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="col-md-1">
                <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
            </div>
        </div>
        <table class="table table-bordered" id="item_table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>{{trans('text.ItemName')}}</th>
                    <th>{{ trans('text.Price') }}</th>
                    <th>{{ trans('text.createdAt') }}</th>
                    <th width="25%">{{ trans('text.Action') }}</th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <input type="text" data-column="1" placeholder="{{trans('text.ItemName')}}...." class="form-control filter-input">
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <div id="formModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"> {{ trans('text.Add') }} {{ trans('text.Item') }}</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <form method="post" id="form_item" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="control-label col-md-4" >{{ trans('text.Name') }} : </label>
                            <div class="col-md-12">
                                <input type="text" name="name" id="name" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">{{ trans('text.Price') }} : </label>
                            <div class="col-md-12">
                                <input type="text" name="price" id="price" class="form-control" />
                            </div>
                        </div>
                        <br />
                        <div class="form-group" align="center">
                            <input type="hidden" name="action" id="action" value="Add" />
                            <input type="hidden" name="hidden_id" id="hidden_id" />
                            <input type="submit" name="action_button" id="action_button" class="btn btn-success" value="Add Item" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="confirmModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>{{ trans('text.Confirmation') }}</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <p align="center" style="margin:0;">{{ trans('text.deleteConf') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('text.Cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function(){

            load_data();

            function load_data(timezone = ''){
                var t = $('#item_table').DataTable({
                    processing: true,
                    serverSide: true,
                    bLengthChange: false,
                    ajax: {
                        url: "{{ route('items.index') }}",
                        data: {timezone:timezone}
                    },
                    columns: [
                        {
                            data: 'rownum',
                            name: 'rownum'
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'price',
                            name: 'price'
                        },
                        {
                            data: 'created_at',
                            name: 'created_at'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false
                        }
                    ],
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]]
                });

                $('.filter-input').keyup(function(){
                    t.column( $(this).data('column'))
                        .search($(this).val())
                        .draw();
                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();
            };

            $('#filter').click(function() {
                var timezone = $('#timezone').val();
                if(timezone != ''){
                    $('#item_table').DataTable().destroy();
                    load_data(timezone);
                };
            });  

            $('#create_record').click(function(){
                $('.modal-title').text('{{trans("text.Add")}} {{trans("text.Item")}}');
                $('#action_button').val('{{trans("text.Add")}}');
                $('#action').val('Add');
                $('#form_result').html('');
                $('#formModal').modal('show');
                $('#name').val(null);
                $('#price').val(null);
            });

            $('#form_item').on('submit', function(event){
                event.preventDefault();
                var action_url = '';
                var action_method = '';

                if($('#action').val() == 'Add') {
                    action_url = "{{ route('items.store') }}";
                    action_method = "POST";
                }

                if($('#action').val() == 'Edit') {
                    action_url = "{{ route('items.update', ':hidden_id') }}";
                    action_method = "PUT";
                }


                $.ajax({
                    url: action_url,
                    method:action_method,
                    data:$(this).serialize(),
                    dataType:"json",
                    success:function(data) {
                        var html = '';
                        if(data.errors) {
                            html = '<div class="alert alert-danger">';
                            for(var count = 0; count < data.errors.length; count++) {
                                html += '<p>' + data.errors[count] + '</p>';
                            }
                            html += '</div>';
                        }
                        if(data.success) {
                            html = '<div class="alert alert-success">' + data.success + '</div>';
                            $('#form_item')[0].reset();
                            $('#item_table').DataTable().ajax.reload();
                        }
                        $('#form_result').html(html);
                    }
                });
            });

            $(document).on('click', '.edit', function(){
                var id = $(this).attr('id');
                $('#form_result').html('');
                $.ajax({
                    url :"/items/"+id+"/edit",
                    dataType:"json",
                    success:function(data) {
                        $('#name').val(data.result.name);
                        $('#price').val(data.result.price);
                        $('#hidden_id').val(id);
                        $('.modal-title').text('{{trans("text.Edit")}} {{trans("text.Item")}}');
                        $('#action_button').val('{{trans("text.Edit")}}');
                        $('#action').val('Edit');
                        $('#formModal').modal('show');
                    }
                })
            });

            var item_id;

            $(document).on('click', '.delete', function(){
                item_id = $(this).attr('id');
                $('#confirmModal').modal('show');
            });

            $('#ok_button').click(function(){
                $.ajax({
                    url:"item/destroy/"+item_id,
                    success:function(data) {
                        setTimeout(function(){
                            $('#confirmModal').modal('hide');
                            $('#item_table').DataTable().ajax.reload();
                            alert('Data Deleted');
                        }, 2000);
                    }
                })
            });
        });
</script>
@stop