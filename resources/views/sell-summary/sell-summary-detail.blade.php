@extends('adminlte::page')

@section('title', trans('text.SellSummaryDetail'))

@section('content_header')
    <h1>{{ trans('text.SellSummaryDetail') }}: {{ $sell_summary->hasEmployee->first_name }} {{ $sell_summary->hasEmployee->last_name }} ({{ $sell_summary->date }})</h1>
@stop

@section('content')<!doctype html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    @foreach($sells as $sell)
        <table>
            <tr>
                <th>{{ $loop->iteration }}. {{ trans('text.ItemName') }}</th>
                <th> : </th>
                <th>{{$sell->hasItem->name}}</th>
            </tr>
            <tr>
                <th>{{ trans('text.Price') }}</th>
                <th> : </th>
                <td>@convert($sell->price)</td>
            </tr>
            <tr>
                <th>{{ trans('text.Discount') }}</th>
                <th> : </th>
                <td>{{$sell->discount}}</td>
            </tr>
            <tr>
                <th>{{ trans('text.createdAt') }}</th>
                <th> : </th>
                <td>{{ $sell->created_date }}</td>
            </tr>
        </table>
        <br/>
    @endforeach

    <br/>
    <table>
        <tr>
            <th>{{ trans('text.PriceTotal') }}</th>
            <th> : </th>
            <td>@convert($sell_summary->price_total)</td>
        </tr>
        <tr>
            <th>{{ trans('text.DiscountTotal') }}</th>
            <th> : </th>
            <td>@convert($sell_summary->discount_total)</td>
        </tr>
        <tr>
            <th>{{ trans('text.Total') }}</th>
            <th> : </th>
            <td>@convert($sell_summary->total)</td>
        </tr>
    </table>

@stop

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
@stop