@extends('adminlte::page')

@section('title', trans('text.SellSummary'))

@section('content_header')
    <h1>{{ trans('text.SellSummary') }}</h1>
@stop

@section('content')<!doctype html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <hr>

    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <select data-column="5" class="form-control filter-select" name="timezone" id="timezone">
                    <option value="">{{ trans('text.Timezone') }}...</option>
                    @foreach($timezones as $timezone)
                        @if(session::get('timezone_id') == $timezone->id)
                            <option value="{{ $timezone->id }}" selected>{{ $timezone->name }} ({{ $timezone->offset }})</option>
                        @else
                            <option value="{{ $timezone->id }}">{{ $timezone->name }} ({{ $timezone->offset }})</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="col-md-1">
                <button type="button" name="filter_timezone" id="filter_timezone" class="btn btn-primary">Filter</button>
            </div>
        </div>
        <br/>

        <!-- Date Range, Employee and Company Filter -->
        <div class="row input-daterange">
            <div class="row">
                <div class="col-md-4">
                    <input type="text" name="from_date" id="from_date" class="form-control" placeholder="{{ trans('text.FromDate') }}" readonly />
                </div>
                <div class="col-md-4">
                    <input type="text" name="to_date" id="to_date" class="form-control" placeholder="{{ trans('text.ToDate') }}" readonly />
                </div>
            </div>
            
            <br/><br/>

            <div class="row">
                <div class="col-md-4">
                    <select data-column="5" class="form-control filter-select" name="employee" id="employee">
                        <option value="">{{ trans('text.ChooseEmployee') }}...</option>
                        @foreach($employees as $employee)
                            <option value="{{ $employee->id }}">{{ $employee->first_name }} {{ $employee->last_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-4">
                    <select data-column="5" class="form-control filter-select" name="company" id="company">
                        <option value="">{{ trans('text.ChooseCompany') }}...</option>
                        @foreach($companies as $company)
                            <option value="{{ $company->id }}">{{ $company->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            
            <br/><br/>
            
            <div class="row">
                <div class="col-md-4">
                    <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                    <button type="button" name="refresh" id="refresh" class="btn btn-secondary">Reset</button>
                </div>
            </div>
        </div>
        <br/>

        <table class="table table-bordered" id="sell_summary_table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>{{ trans('text.Date') }}</th>
                    <th>{{ trans('text.Employee') }}</th>
                    <th>{{ trans('text.Company') }}</th>
                    <th>{{ trans('text.PriceTotal') }}</th>
                    <th>{{ trans('text.DiscountTotal') }}</th>
                    <th>{{ trans('text.Total') }}</th>
                    <th>{{ trans('text.createdAt') }}</th>
                    <th width="20%">{{ trans('text.Action') }}</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
@endsection

@section('footer')

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
@stop

@section('js')
    <script> console.log('Hi!'); </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
    <script>
        $(document).ready(function(){
            $('.input-daterange').datepicker({
                todayBtn:'linked',
                format:'yyyy-mm-dd',
                autoclose:true
            });

            load_data();

            function load_data(timezone = '', from_date = '', to_date = '', employee = '', company = ''){
                var t = $('#sell_summary_table').DataTable({
                    pageLength: 10, //Default 10 per page
                    processing: true,
                    serverSide: true,
                    bLengthChange: false,
                    ajax: {
                        url: "{{ route('sell-summary.index') }}",
                        data: {timezone:timezone, from_date:from_date, to_date:to_date, employee:employee, company:company}
                    },
                    columns: [
                        {
                            data: 'rownum',
                            name: 'rownum'
                        },
                        {
                            data: 'date',
                            name: 'date'
                        },
                        {
                            data: 'employee',
                            name: 'employee'
                        },
                        {
                            data: 'company',
                            name: 'company'
                        },
                        {
                            data: 'price_total',
                            name: 'price_total'
                        },
                        {
                            data: 'discount_total',
                            name: 'discount_total'
                        },
                        {
                            data: 'total',
                            name: 'total',
                        },
                        {
                            data: 'created_date',
                            name: 'created_date',
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false
                        }
                    ],
                    "columnDefs": [ {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    } ],
                    "order": [[ 1, 'asc' ]]
                });

                $('.filter-input').keyup(function(){
                    t.column( $(this).data('column'))
                        .search($(this).val())
                        .draw();
                });

                t.on( 'order.dt search.dt', function () {
                    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    } );
                } ).draw();
            };

            $('#filter_timezone').click(function() {
                var timezone = $('#timezone').val();
                if(timezone != ''){
                    $('#sell_summary_table').DataTable().destroy();
                    load_data(timezone, null, null, null, null);
                };
            });

            $('#refresh_timezone').click(function(){
                $('#timezone').val('');
                $('#sell_summary_table').DataTable().destroy();
                load_data();
            });

            $('#filter').click(function () {
                var from_date = $('#from_date').val();
                var to_date = $('#to_date').val();
                var employee = $('#employee').val();
                var company = $('#company').val();
                $('#sell_summary_table').DataTable().destroy();
                load_data(null, from_date, to_date, employee, company);
            });

            $('#refresh').click(function(){
                $('#from_date').val('');
                $('#to_date').val('');
                $('#employee').val('');
                $('#company').val('');
                // session->forget('employee');
                // session->forget('company');
                $('#sell_summary_table').DataTable().destroy();
                load_data();
            });
        });
    </script>
@stop