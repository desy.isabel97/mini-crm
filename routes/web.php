<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CompaniesController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ItemsController;
use App\Http\Controllers\SellController;
use App\Http\Controllers\SellSummaryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'App\Http\Controllers\LanguageController@switchLang']);

Auth::routes(['register' => false, 'reset' => false]);

Route::group(['middleware' => 'auth', 'checktoken'], function () {
    Route::get('/', [CompaniesController::class, 'index']); 
    Route::get('home', [CompaniesController::class, 'index']); 

    Route::get('/data-companies', [CompaniesController::class, 'dataCompanies']);

    Route::resource('companies', CompaniesController::class);

    Route::get('company/destroy/{id}', [CompaniesController::class, 'destroy']);


    Route::get('/data-employees', [EmployeesController::class, 'dataEmployees']);

    Route::resource('employees', EmployeesController::class);

    Route::get('employee/destroy/{id}', [EmployeesController::class, 'destroy']);


    Route::resource('items', ItemsController::class);

    Route::get('item/destroy/{id}', [ItemsController::class, 'destroy']);


    Route::resource('sell', SellController::class);

    Route::get('sell/destroy/{id}', [SellController::class, 'destroy']);

    Route::get('store-item-price/{id}', [SellController::class, 'storeItemPrice']);

    Route::resource('sell-summary', SellSummaryController::class);
});
