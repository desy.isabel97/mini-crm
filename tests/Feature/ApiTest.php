<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Companies;
use App\Models\Employees;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;
use Database\Factories\CompaniesFactory;
use Illuminate\Support\Facades\Hash;


class ApiTest extends TestCase
{
    use WithFaker; 

    /**
     * Test can login with valid token
     *
     * @test
     * @return void
     */
    public function test_login_with_valid_token()
    {
        $user = User::factory()->create([
            'email' => 'admin123@mail.com',
            'password' => Hash::make(123456),
        ]);

        $response = $this->post(
            'api/login',
            [
                'email' => $user->email,
                'password' => "123456"
            ]
        );
        $response->assertOk();
    }

    /**
     * Test to get employees list by company id.
     */
    public function test_get_employees_list_by_company()
    {
        $user = User::factory()->create([
            'email' => 'admin1234@mail.com',
            'password' => Hash::make(123456),
        ]);

        $response = $this->post(
            'api/login',
            [
                'email' => $user->email,
                'password' => "123456"
            ]
        );
        $token = $response->original['token'];
        
        // $admin = User::factory()->create();

        // create company and employee relation
        $company = Companies::factory()->create([
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);
        Employees::factory()->create([
            'company' => $company->id,
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);

        // authentication token
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Bearer' => $token,
        ])->json(
            'POST',
            'api/employeesByCompany',
            ['company_id' => $company->id]
        );

        $response->assertStatus(200);
    }
}
