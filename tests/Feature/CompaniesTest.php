<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Companies;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;
use Database\Factories\CompaniesFactory;

class CompaniesTest extends TestCase
{
    use WithFaker; 

    /**
     * create user login with jwt.
     */
    public function logged_in_user()
    {
        $admin = User::factory()->create();

        $response = $this->actingAs($admin)->withSession(['token' => $admin]);
        return $response;
    }

    /**
     * Test user can access companies page.
     *
     * @return void
     */
    public function test_admin_can_access_companies_page()
    {
        $response = $this->get('/companies')
                    ->assertRedirect('/login');
    }

    /**
     * Test store function.
     *
     * @return void
     */
    // }
    public function test_store()
    {
        $this->withoutMiddleware();
        
        $name = 'Water Blue Corp.';
        $value = $this->dummy_data($name);
        $response = $this->logged_in_user()->post(route('companies.store'), $value);
        $response->assertSessionHas('success', null);
    }

    /**
     * Test update function.
     *
     * @return void
     */
    public function test_update()
    {
        $this->withoutMiddleware();

        $company = Companies::factory()->create();

        $name = 'Water Blue Corporate';
        $value = $this->dummy_data($name);

        $response = $this->logged_in_user()->put(route('companies.update', $company->id), [
            'name' => $name,
            'email' => 'waterblue@corporate.com', //update
            'website' => 'waterblue.corp',
            'logo' => "/public/logo/avatar.jpg",
            'domain' => 'waterblue'
        ]);
        $response->assertSessionHas('success', null);
    }

    /**
     * dummy data for store test and update test.
     *
     * @return void
     */
    // }
    public function dummy_data($name){
        return [
            'name' => $name,
            'email' => 'waterblue@mail.com',
            'website' => 'waterblue.corp',
            'logo' => "/public/logo/avatar.jpg",
            'domain' => 'waterblue'
        ];
    }
}
