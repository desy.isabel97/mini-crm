<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Employees;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Input;
use Database\Factories\EmployeesFactory;

class EmployeesTest extends TestCase
{
    /**
     * create user login with jwt.
     *
     * @return void
     */
    public function logged_in_user()
    {
        $admin = User::factory()->create();

        $response = $this->actingAs($admin)->withSession(['token' => $admin]);
        return $response;
    }

    /**
     * Test user can access employees page.
     *
     * @return void
     */
    public function test_admin_can_access_employees_page()
    {
        $response = $this->get('/employees')
                    ->assertRedirect('/login');
    }

    /**
     * Test store function.
     *
     * @return void
     */
    public function test_store()
    {
        $this->withoutMiddleware();
        $email = 'putra.willy@mail.com';
        $value = $this->dummy_data($email);
        $response = $this->logged_in_user()->post(route('employees.store'), $value);
        $response->assertOk();
    }

    /**
     * Test update function.
     *
     * @return void
      */
     
    public function test_update()
    {
        $this->withoutMiddleware();

        $employee = Employees::factory()->create();

        $email = 'willyputra@mail.com';
        $value = $this->dummy_data($email);

        $response = $this->logged_in_user()->put(route('employees.update', $employee->id), 
            [
                'first_name' => 'Willy',
                'last_name' => 'Putra S.', //update
                'email' => $email,
                'phone' => '087866493822', //update
                'company' => 2,
                'hidden_id' => $employee->id
            ]
            );
        $response->assertOk();
    } 

    /**
     * dummy data for store test and update test.
     *
     * @return void
     */
    // }
    public function dummy_data($email){
        return [
            'first_name' => 'Willy',
            'last_name' => 'Putra',
            'email' => $email,
            'phone' => '081212992876',
            'company' => 2,
        ];
    }
}
