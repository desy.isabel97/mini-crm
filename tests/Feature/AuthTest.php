<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class AuthTest extends TestCase
{
    /**
     * Test login with jwt
     */
    public function test_admin_login_with_jwt()
    {
        $admin = User::factory()->create();

        $response = $this->actingAs($admin)->withSession(['token' => $admin])->get('companies');
        $response->assertOk();

    }
}
