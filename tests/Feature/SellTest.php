<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Database\Factories\SellFactory;
use App\Models\Sell;
use App\Models\Items;
use App\Models\User;

class SellTest extends TestCase
{
    /**
     * create user login with jwt.
     *
     * @return void
     */
    public function logged_in_user()
    {
        $admin = User::factory()->create();

        $response = $this->actingAs($admin)->withSession(['token' => $admin]);
        return $response;
    }

    /**
     * Test user can access sell page.
     */
    public function test_admin_can_access_sell_page()
    {
        $response = $this->get('/sell')
                    ->assertRedirect('/login');
    }

    /**
     * Test store function and create sell summary data.
     */
    public function test_store_with_sell_summary()
    {
        $this->withoutMiddleware();
        
        $value = $this->dummy_data();
        $response = $this->logged_in_user()->post(route('sell.store'), $value);
        $response->assertSessionHas('success', null);
    }

    /**
     * Test update function.
     */
    public function test_update()
    {
        $this->withoutMiddleware();

        $sell = Sell::factory()->create(['employee' => 1]);

        $response = $this->logged_in_user()->PUT('sell/' . $sell->id, [
            'employee' => 2,
            'price' => 60000
        ]);
        $response->assertRedirect();
    }

    /**
     * Test delete function.
     */
    public function test_delete()
    {
        $this->withoutMiddleware();

        $sell = Sell::factory()->create(['employee' => 1]);

        $response = $this->logged_in_user()->DELETE('sell/' . $sell->id);
        // $response->dump();
        $response->assertRedirect();
    }

    /**
     * dummy data for store test.
     */
    public function dummy_data()
    {

        return [
            'item' => 1,
            'price' => 10000, 
            'discount' => 0.03,
            'employee' => 1,
        ];
    }
}
