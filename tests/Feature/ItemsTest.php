<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Database\Factories\ItemsFactory;
use App\Models\Items;
use App\Models\User;

class ItemsTest extends TestCase
{
    /**
     * create user login with jwt.
     *
     * @return void
     */
    public function logged_in_user()
    {
        $admin = User::factory()->create();

        $response = $this->actingAs($admin)->withSession(['token' => $admin]);
        return $response;
    }

    /**
     * Test user can access items page.
     */
    public function test_admin_can_access_items_page()
    {
        $response = $this->get('/items')
                    ->assertRedirect('/login');
    }

    /**
     * Test store function.
     */
    public function test_store()
    {
        $this->withoutMiddleware();
        
        $value = $this->dummy_data();
        $response = $this->logged_in_user()->post(route('items.store'), $value);
        $response->assertOk();
    }

    /**
     * Test update function.
     */
    public function test_update()
    {
        $this->withoutMiddleware();

        $item = Items::factory()->create();

        $response = $this->logged_in_user()->PATCH('items/update' . $item->id, [
            // 'hidden_id' => $employee->id,
            'price' => 60000
        ]);
        $response->assertRedirect();
    }

    /**
     * Test delete function.
     */
    public function test_delete()
    {
        $this->withoutMiddleware();

        $item = Items::factory()->create();

        $response = $this->logged_in_user()->DELETE('items/' . $item->id);
        $response->assertOk();
    }

    /**
     * Dummy data for store test.
     */
    public function dummy_data(){
        return [
            'name' => 'Item 7',
            'price' => 70000,
        ];
    }
}
