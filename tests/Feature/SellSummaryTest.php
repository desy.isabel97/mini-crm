<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Database\Factories\SellFactory;
use App\Models\Sell;
use App\Models\Items;
use App\Models\User;
use App\Models\Employees;

class SellSummaryTest extends TestCase
{
    /**
     * create user login with jwt.
     *
     * @return void
     */
    public function logged_in_user()
    {
        $admin = User::factory()->create();

        $response = $this->actingAs($admin)->withSession(['token' => $admin]);
        return $response;
    }

    /**
     * Test user can access sell summary page.
     */
    public function test_admin_can_access_sell_summary_page()
    {
        $response = $this->get('/sell-summary')
                    ->assertRedirect('/login');
    }
}
